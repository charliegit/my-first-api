package com.folcademy.myfirstapi.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodbyeWorldController {
@PostMapping("/goodbye")
    public String goodbyeWorld ()
{
    return "Goodbye World";
}
}
